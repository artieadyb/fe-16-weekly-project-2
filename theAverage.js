const theAverage = (number) => {
    let sum = 0;
    for(let i = 0; i < number.length; i++){
        sum += number[i];
    }
    let average = sum / number.length;
    return average;
};

const input = [9, 10, 3, 20, 7];

console.log(theAverage(input));
