const oddEven = (number) => {
if (number % 2 === 0){
    return `${number} is even number`;
}else {
    return `${number} is odd number`;
};
};

const input = 6;

console.log(oddEven(input));
