const fizzBuzz = (number) => {
    let b = "" ;
    for(let a = 1; a <= number; a++){
        if(a % 3 === 0 && a % 5 === 0){
            b += "FizzBuzz ";
        }else if(a % 3 === 0){
            b+="Fizz ";
        }else if(a % 5 === 0){
            b+="Buzz ";
        }else{
            b +=`${a} `;
        };
    };
    return b;
};

const input = 15;

console.log(fizzBuzz(input));
