const capitalToFront = (word) => {
    let wordSplit = word.split("");
    let low =[];
    let cap = [];
    let join = [];

    for (let i=0; i<wordSplit.length; i++){
        if (wordSplit[i] === wordSplit[i].toUpperCase()){
            cap += wordSplit[i];
        }else{
            low += wordSplit[i];
        }
    }
    join = [...cap,...low];
    return join.join("")
};

const input = "cAcAttuUU";

console.log(capitalToFront(input));
