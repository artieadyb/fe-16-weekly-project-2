const countWord = (word, phrases) => {
    return (phrases.toLowerCase().split(word.toLowerCase()).length - 1)
};

const wordToCount = "cat";
const input = "cat dog cat cat cat dog";

console.log(countWord(wordToCount, input));