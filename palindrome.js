const palindrome = (text) => {
    let a = text.split("");
    let b = a.reverse();
    let c = b.join("");
    return c === text;
};

const input = "Hello";

console.log(palindrome(input));