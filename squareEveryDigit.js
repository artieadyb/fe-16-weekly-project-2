const squareEveryDigit = (number) => {
    let angka = number.split("");
    let square = [];
    for (let i=0; i<angka.length; i++){
        square.push(Math.pow(angka[i],2));
    }
    let hasil = square.join("")
    return hasil;
};

const input = "5349";

console.log(squareEveryDigit(input));
